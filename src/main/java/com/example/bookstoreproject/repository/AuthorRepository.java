package com.example.bookstoreproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.example.bookstoreproject.model.Author;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long>{
	
	List<Author> findByFirstName(String firstName);

	@Query(value = "SELECT * FROM Author", nativeQuery=true)
	List<Author> findAllAuthor();
	
	@Modifying
	@Query
	(value = "INSERT INTO Author( age, country, first_Name, gender, last_Name, rating) VALUES (:age, :country, :firstName, :gender, :lastName, :rating)", nativeQuery=true)
	@Transactional
	void saveAuthor(@Param("age") Integer age, @Param("country") String country, 
					@Param("firstName") String firstName,  @Param("gender") String gender,  
					@Param("lastName") String lastName,  @Param("rating") String rating
			       );
	
	@Modifying
	@Query
	(value = "DELETE FROM author Where author_id = :id", nativeQuery = true)
	@Transactional
	void deleteAuthor(@Param("id") Long id);
	
}
