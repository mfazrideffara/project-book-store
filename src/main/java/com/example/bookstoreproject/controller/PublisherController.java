package com.example.bookstoreproject.controller;

import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.bookstoreproject.exception.ResourceNotFoundException;
import com.example.bookstoreproject.model.Publisher;
import com.example.bookstoreproject.repository.PublisherRepository;

@RestController
@RequestMapping("/api")
public class PublisherController {

    @Autowired
    PublisherRepository publisherRepository;

 // Get All Publisher
    @GetMapping("/publisher")
    public HashMap<String, Object> getAllPublisher() {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	List<Publisher> listPublisher = publisherRepository.findAll();
    	String message;
    	if(listPublisher.isEmpty()) {
    		message = "Read All Failed!";
    	} else {
    		message = "Read All Success!";
    	}
    	showHashMap.put("Message", message);
    	showHashMap.put("Total", listPublisher.size());
    	showHashMap.put("Data", listPublisher);
    	
    	return showHashMap;
    }
    
    // Create a new Publisher
    @PostMapping("/publisher")
    public HashMap<String, Object> createPublisher(@Valid @RequestBody Publisher... publisher) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	@Valid Publisher[] listPublisher = publisher;
    	String message;
    	
    	for(Publisher p : listPublisher) {
    		publisherRepository.save(p);
    	}
    
    	if(listPublisher == null) {
    		message = "Create Failed!";
    	} else {
    		message = "Create Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Insert", listPublisher.length);
    	showHashMap.put("Data", listPublisher);
    	
    	return showHashMap;
    }
    
    // Get a Single Publisher
    @GetMapping("/publisher/{id}")
    public Publisher getPublisherById(@PathVariable(value = "id") Long id) {
        return publisherRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Publisher", "id", id));
    }
    
    // Update a Publisher
    @PutMapping("/publisher/{id}")
    public Publisher updatePublisher(@PathVariable(value = "id") Long id,
                                            @Valid @RequestBody Publisher publisherDetails) {

    	Publisher publisher = publisherRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Publisher", "id", id));

    	publisher.setPublisherName(publisherDetails.getPublisherName());;
    	publisher.setCountry(publisherDetails.getCountry());
    	publisher.setPaper_id(publisherDetails.getPaper_id());

    	Publisher updatePublisher = publisherRepository.save(publisher);
        return updatePublisher;
    }
    
    // Delete a Publisher
    @DeleteMapping("/publisher/{id}")
    public ResponseEntity<?> deletePublisher(@PathVariable(value = "id") Long id) {
    	Publisher publisher = publisherRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Publisher", "id", id));

    	publisherRepository.delete(publisher);

        return ResponseEntity.ok().build();
    }
    
    // Sort Publisher by publisherName
    @GetMapping("/publisher/sort")
	public List<Publisher> sortAuthor(@RequestParam(value="publisherName")String publisherName){
		return publisherRepository.findByPublisherName(publisherName);
	}
}

