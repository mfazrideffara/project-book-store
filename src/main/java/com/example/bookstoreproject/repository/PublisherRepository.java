package com.example.bookstoreproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.bookstoreproject.model.Publisher;

@Repository
public interface PublisherRepository extends JpaRepository<Publisher, Long>{

	List<Publisher> findByPublisherName(String publisherName);

}