package com.example.bookstoreproject.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.bookstoreproject.exception.ResourceNotFoundException;
import com.example.bookstoreproject.model.PaperQuality;
import com.example.bookstoreproject.repository.PaperQualityRepository;

@RestController
@RequestMapping("/api")
public class PaperQualityController {

    @Autowired
    PaperQualityRepository paperQualityRepository;

 // Get All PaperQuality
    @GetMapping("/paperQuality")
    public HashMap<String, Object> getAllPaperQuality() {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	List<PaperQuality> listPaperQuality = paperQualityRepository.findAll();
    	String message;
    	if(listPaperQuality.isEmpty()) {
    		message = "Read All Failed!";
    	} else {
    		message = "Read All Success!";
    	}
    	showHashMap.put("Message", message);
    	showHashMap.put("Total", listPaperQuality.size());
    	showHashMap.put("Data", listPaperQuality);
    	
    	return showHashMap;
    }
    
    // Create a new PaperQuality
    @PostMapping("/paperQuality")
    public HashMap<String, Object> createPaperQuality(@Valid @RequestBody PaperQuality... paperQuality) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	@Valid PaperQuality[] listPaperQuality = paperQuality;
    	String message;
    	
    	for(PaperQuality pq : listPaperQuality) {
    		paperQualityRepository.save(pq);
    	}
    
    	if(listPaperQuality == null) {
    		message = "Create Failed!";
    	} else {
    		message = "Create Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Insert", listPaperQuality.length);
    	showHashMap.put("Data", listPaperQuality);
    	
    	return showHashMap;
    }
    
    // Get a Single PaperQuality
    @GetMapping("/paperQuality/{id}")
    public PaperQuality getPaperQualityById(@PathVariable(value = "id") Long id) {
        return paperQualityRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("PaperQuality", "id", id));
    }
    
    // Update a PaperQuality
    @PutMapping("/paperQuality/{id}")
    public HashMap<String, Object> updatePaperQuality(@PathVariable(value = "id") Long id,
            @Valid @RequestBody PaperQuality paperQualityDetails) {
    	
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	String message;
    	
    	int paper_id = id.intValue();
    	List<PaperQuality> listPaper = paperQualityRepository.findAll();
    	
    	for(PaperQuality pq : listPaper) {
    		if(pq.getPaper_Id() == paper_id) {
    			if(paperQualityDetails.getPrice() == 0) {
    	    		paperQualityDetails.setPrice(listPaper.get(paper_id).getPrice());
    	    	}
    	    	if(paperQualityDetails.getQuality() == null) {
    	    		paperQualityDetails.setQuality(listPaper.get(paper_id).getQuality());
    	    	}
    		}
    	}	
    	
    	PaperQuality paperQuality = paperQualityRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("PaperQuality", "id", id));

    	paperQuality.setPrice(paperQualityDetails.getPrice());
    	paperQuality.setQuality(paperQualityDetails.getQuality());

    	PaperQuality updatePaper = paperQualityRepository.save(paperQuality);
    	
    	List<PaperQuality> resultList = new ArrayList<PaperQuality>();
    	resultList.add(updatePaper);
    	
    	if(resultList.isEmpty()) {
    		message = "Update Failed!";
    	} else {
    		message = "Update Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Update", resultList.size());
    	showHashMap.put("Data", resultList);
    	
    	return showHashMap;
    }
    
    // Delete a PaperQuality
    @DeleteMapping("/paperQuality/{id}")
    public ResponseEntity<?> deletePaperQuality(@PathVariable(value = "id") Long id) {
    	PaperQuality PaperQuality = paperQualityRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("PaperQuality", "id", id));

    	paperQualityRepository.delete(PaperQuality);

        return ResponseEntity.ok().build();
    }
}
