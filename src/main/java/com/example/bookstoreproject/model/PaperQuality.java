package com.example.bookstoreproject.model;

import java.io.Serializable;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@Entity
@JsonIgnoreProperties(value = {"paper_id"}, 
        allowGetters = true)
public class PaperQuality implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long paper_id;

    private String Quality;
    
    private double price;
    
    
    @OneToMany(mappedBy = "paper_id", cascade = CascadeType.ALL , fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<Publisher> publishers;

    public PaperQuality(Publisher... publishers) {
        this.publishers = Stream.of(publishers).collect(Collectors.toSet());
        this.publishers.forEach(x -> x.setPaper_id(this));
    }
    
    public PaperQuality() {
    	
    }
    
	public Long getPaper_Id() {
		return paper_id;
	}
	
	public String getQuality() {
		return Quality;
	}

	public void setPaper_Id(Long paper_id) {
		this.paper_id = paper_id;
	}

	public void setQuality(String quality) {
		Quality = quality;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
}	
