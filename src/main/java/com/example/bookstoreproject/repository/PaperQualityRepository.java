package com.example.bookstoreproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.bookstoreproject.model.PaperQuality;

@Repository
public interface PaperQualityRepository extends JpaRepository<PaperQuality, Long>{
	
}
