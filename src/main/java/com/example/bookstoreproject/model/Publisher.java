package com.example.bookstoreproject.model;

import java.io.Serializable;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@JsonIgnoreProperties(value = {"publisher_id"}, 
        allowGetters = true)

public class Publisher implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long publisher_id;

    private String publisherName;
    
    private String country;
    
    @ManyToOne
    @JoinColumn
    private PaperQuality paper_id;
    
    
    @OneToMany(mappedBy = "publisher", cascade = CascadeType.ALL , fetch = FetchType.LAZY)
    @JsonIgnore	
    private Set<Book> books;

    public Publisher(Book... books) {
        this.books = Stream.of(books).collect(Collectors.toSet());
        this.books.forEach(x -> x.setPublisher(this));
    }
    
    public Publisher() {
    	
    }
    
	public Long getPublisher_id() {
		return publisher_id;
	}

	public String getPublisherName() {
		return publisherName;
	}

	public String getCountry() {
		return country;
	}
	
	public PaperQuality getPaper_id() {
		return paper_id;
	}

	public Set<Book> getBooks() {
		return books;
	}

	public void setPublisher_id(Long publisher_id) {
		this.publisher_id = publisher_id;
	}

	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setPaper_id(PaperQuality paper_id) {
		this.paper_id = paper_id;
	}

	public void setBooks(Set<Book> books) {
		this.books = books;
	}
}