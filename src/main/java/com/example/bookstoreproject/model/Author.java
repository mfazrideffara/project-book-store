package com.example.bookstoreproject.model;

import java.io.Serializable;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties(value = {"author_id"}, 
        allowGetters = true)
public class Author implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long author_id;

    private String firstName;

    private String lastName;
    
    private String gender;
    
    private String country;
    
    private int age;

    private String rating;
    
    
    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<Book> books;

    public Author(Book... books) {
        this.books = Stream.of(books).collect(Collectors.toSet());
        this.books.forEach(x -> x.setAuthor(this));
    }
    
    public Author() {
    	
    }
    
    //Method Setter Getter
	public Long getId() {
		return author_id;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getGender() {
		return gender;
	}

	public String getCountry() {
		return country;
	}

	public int getAge() {
		return age;
	}

	public String getRating() {
		return rating;
	}

	public void setId(Long author_id) {
		this.author_id = author_id;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}
}