package com.example.bookstoreproject.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.bookstoreproject.exception.ResourceNotFoundException;
import com.example.bookstoreproject.model.Author;
import com.example.bookstoreproject.repository.AuthorRepository;

@RestController
@RequestMapping("/api")
public class AuthorController {

    @Autowired
    AuthorRepository authorRepository;

    //Get All Author
    @GetMapping("/author")
    public HashMap<String, Object> getAllAuthor() {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	List<Author> listAuthors = authorRepository.findAllAuthor();
    	String message;
    	if(listAuthors.isEmpty()) {
    		message = "Read All Failed!";
    	} else {
    		message = "Read All Success!";
    	}
    	showHashMap.put("Message", message);
    	showHashMap.put("Total", listAuthors.size());
    	showHashMap.put("Data", listAuthors);
    	
    	return showHashMap;
    }
    
    // Create a new Author
    @PostMapping("/author")
    public HashMap<String, Object> createAuthor(@Valid @RequestBody Author... author) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	@Valid Author[] listAuthors = author;
    	String message;
    	
    	for(Author a : listAuthors) {
    		authorRepository.saveAuthor(a.getAge(),
    									a.getCountry(),
    									a.getFirstName(),
    									a.getGender(),
    									a.getLastName(),
    									a.getRating());
    	}
    
    	if(listAuthors == null) {
    		message = "Create Failed!";
    	} else {
    		message = "Create Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Insert", listAuthors.length);
    	showHashMap.put("Data", listAuthors);
    	
    	return showHashMap;
    }
    
    // Get a Single Author
    @GetMapping("/author/{id}")
    public Author getAuthorById(@PathVariable(value = "id") Long id) {
        return authorRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Author", "id", id));
    }
    
    // Update a Author
    @PutMapping("/author/{id}")
    public HashMap<String, Object> updateAuthor(@PathVariable(value = "id") Long id,
            @Valid @RequestBody Author authorDetails) {
    	
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	String message;
    	
    	int authorId = id.intValue();
    	List<Author> listAuthors = authorRepository.findAll();
    	
    	for(Author a : listAuthors) {
    		if(a.getId() == authorId) {
    			if(authorDetails.getAge() == 0) {
    	    		authorDetails.setAge(listAuthors.get(authorId).getAge());
    	    	}
    	    	if(authorDetails.getCountry() == null) {
    	    		authorDetails.setCountry(listAuthors.get(authorId).getCountry());
    	    	}
    	    	if(authorDetails.getFirstName() == null) {
    	    		authorDetails.setFirstName(listAuthors.get(authorId).getFirstName());
    	    	}
    	    	if(authorDetails.getGender() == null) {
    	    		authorDetails.setGender(listAuthors.get(authorId).getGender());
    	    	}
    	    	if(authorDetails.getLastName() == null) {
    	    		authorDetails.setLastName(listAuthors.get(authorId).getLastName());
    	    	}
    	    	if(authorDetails.getRating() == null) {
    	    		authorDetails.setRating(listAuthors.get(authorId).getRating());
    	    	}
    	    	
    		}
    	}
    	
    	Author author = authorRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Author", "id", id));
    	
    	author.setAge(authorDetails.getAge());
    	author.setCountry(authorDetails.getCountry());
    	author.setFirstName(authorDetails.getFirstName());
    	author.setGender(authorDetails.getGender());
    	author.setLastName(authorDetails.getLastName());
    	author.setRating(authorDetails.getRating());

    	Author updateAuthor = authorRepository.save(author);
    	
    	List<Author> resultList = new ArrayList<Author>();
    	resultList.add(updateAuthor);
    	
    	if(resultList.isEmpty()) {
    		message = "Update Failed!";
    	} else {
    		message = "Update Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Update", resultList.size());
    	showHashMap.put("Data", resultList);
    	
    	return showHashMap;
    }
    
    // Delete a Author
    @DeleteMapping("/author/{id}")
    public void deleteAuthor(@PathVariable(value = "id") Long id) {
    	
        authorRepository.deleteAuthor(id);
    }
}
