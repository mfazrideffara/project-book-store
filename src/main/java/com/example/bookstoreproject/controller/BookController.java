package com.example.bookstoreproject.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.bookstoreproject.exception.ResourceNotFoundException;
import com.example.bookstoreproject.model.Book;
import com.example.bookstoreproject.repository.BookRepository;

@RestController
@RequestMapping("/api")
public class BookController {

    @Autowired
    BookRepository bookRepository;

 // Get All Book
    @GetMapping("/book")
    public HashMap<String, Object> getAllBook() {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	List<Book> listBook = bookRepository.findAll();
    	String message;
    	if(listBook.isEmpty()) {
    		message = "Read All Failed!";
    	} else {
    		message = "Read All Success!";
    	}
    	showHashMap.put("Message", message);
    	showHashMap.put("Total", listBook.size());
    	showHashMap.put("Data", listBook);
    	
    	return showHashMap;
    }
    
    // Create a new Book
    @PostMapping("/book")
    public HashMap<String, Object> createBook(@Valid @RequestBody Book... book) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	@Valid Book[] listBook = book;
    	String message;
    	
//    	for(Book pq : listBook) {
//    		bookRepository.save(pq);
//    	}
    
    	if(listBook == null) {
    		message = "Create Failed!";
    	} else {
    		message = "Create Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Insert", listBook.length);
    	showHashMap.put("Data", listBook);
    	
    	return showHashMap;
    }
    
    // Get a Single Book
    @GetMapping("/book/{id}")
    public Book getBookById(@PathVariable(value = "id") Long id) {
        return bookRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Book", "id", id));
    }
    
    // Update a Book
    @PutMapping("/book/{id}")
    public HashMap<String, Object> updateBook(@PathVariable(value = "id") Long id,
            @Valid @RequestBody Book bookDetails) {
    	
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	String message;
    	
    	int bookId = id.intValue();
    	List<Book> listBooks = bookRepository.findAll();
    	
    	for(Book a : listBooks) {
    		if(a.getBook_id() == bookId) {
    			if(bookDetails.getPrice() == 0) {
    	    		bookDetails.setPrice(listBooks.get(bookId).getPrice());
    	    	}
    	    	if(bookDetails.getTitleBook() == null) {
    	    		bookDetails.setTitleBook(listBooks.get(bookId).getTitleBook());
    	    	}
    	    	if(bookDetails.getGenre() == null) {
    	    		bookDetails.setGenre(listBooks.get(bookId).getGenre());
    	    	}
    	    	if(bookDetails.getReleaseDate() == null) {
    	    		bookDetails.setReleaseDate(listBooks.get(bookId).getReleaseDate());
    	    	}
    	    	if(bookDetails.getAuthor() == null) {
    	    		bookDetails.setAuthor(listBooks.get(bookId).getAuthor());
    	    	}
    	    	if(bookDetails.getPublisher() == null) {
    	    		bookDetails.setPublisher(listBooks.get(bookId).getPublisher());
    	    	}
    		}
    	}
    	
    	Book book = bookRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("book", "id", id));
    	
    	book.setTitleBook(bookDetails.getTitleBook());
    	book.setGenre(bookDetails.getGenre());
    	book.setReleaseDate(bookDetails.getReleaseDate());
    	book.setPrice(bookDetails.getPrice());
    	book.setAuthor(bookDetails.getAuthor());
    	book.setPublisher(bookDetails.getPublisher());

    	Book updateBook = bookRepository.save(book);
    	
    	List<Book> resultList = new ArrayList<Book>();
    	resultList.add(updateBook);
    	
    	if(resultList.isEmpty()) {
    		message = "Update Failed!";
    	} else {
    		message = "Update Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Update", resultList.size());
    	showHashMap.put("Data", resultList);
    	
    	return showHashMap;
    }
    // Delete a Book
    @DeleteMapping("/book/{id}")
    public ResponseEntity<?> deleteBook(@PathVariable(value = "id") Long id) {
    	Book book = bookRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Book", "id", id));

    	bookRepository.delete(book);

        return ResponseEntity.ok().build();
    }
}