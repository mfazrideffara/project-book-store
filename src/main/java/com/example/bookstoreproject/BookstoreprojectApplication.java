package com.example.bookstoreproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class BookstoreprojectApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookstoreprojectApplication.class, args);
	}

}
