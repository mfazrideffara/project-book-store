package com.example.bookstoreproject.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties(value = {"book_id"}, 
        allowGetters = true)

public class Book implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long book_id;

    private String titleBook;
    
    @ManyToOne
    @JoinColumn
    private Author author;
    
    @ManyToOne
    @JoinColumn
    private Publisher publisher;
    
    private String genre;
    
    private int price;
    
    @CreatedDate
    private Date releaseDate;

	public Long getBook_id() {
		return book_id;
	}

	public String getTitleBook() {
		return titleBook;
	}
	
	public Author getAuthor() {
		return author;
	}
	
	public Publisher getPublisher() {
		return publisher;
	}

	public String getGenre() {
		return genre;
	}

	public int getPrice() {
		return price;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setBook_id(Long book_id) {
		this.book_id = book_id;
	}

	public void setTitleBook(String titleBook) {
		this.titleBook = titleBook;
	}
	
	public void setAuthor(Author author) {
		this.author = author;
	}

	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}    
}